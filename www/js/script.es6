class Cities {
    constructor(root) {
        this.root = root;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {

        }
    }

    _bindEvents() {

    }

    _ready() {

    }
}

class Application {
    constructor() {
        this._mainScripts();
        this._initClasses();
    }

    _mainScripts() {
        let slider = $('.slider__inner').owlCarousel({
            items: 1,
            nav: false,
            dots: false,
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false
        });

        let sliderText = $('.slider__text').owlCarousel({
            items: 1,
            nav: true,
            navText: false,
            dots: false
        });

        sliderText.on('changed.owl.carousel', function(event) {
            var element   = event.target;         // DOM element, in this example .owl-carousel
            var name      = event.type;           // Name of the event, in this example dragged
            var namespace = event.namespace;      // Namespace of the event, in this example owl.carousel
            var items     = event.item.count;     // Number of items
            var item      = event.item.index;     // Position of the current item
            // Provided by the navigation plugin
            var pages     = event.page.count;     // Number of pages
            var page      = event.page.index;     // Position of the current page
            var size      = event.page.size;      // Number of items per page

            slider.trigger('to.owl.carousel', [item, 300]);
        });

        $('.imageSlider__inner').owlCarousel({
            items: 1,
            nav: true,
            navText: false,
            dots: false
        });

        if (document.getElementById('map')) {
            ymaps.ready(function () {
                var myMap = new ymaps.Map('map', {
                        center: [55.693394, 37.556065],
                        zoom: 12
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),

                    myPlacemarkWithContent = new ymaps.Placemark([55.693394, 37.556065]);

                myMap.behaviors.disable('scrollZoom');
                myMap.geoObjects
                    .add(myPlacemarkWithContent);
            });
        }

        flexGridAddElements('masters__inner', 'masters__item', 'masters__item_hide');

        $('body').on('beforeSubmit', '.sendForm', (event) => {
            $.post('/mail/index', $(event.currentTarget).serialize(), function(response) {
                console.log(response);
                if (response == 'success') {
                    alert('Сообщение успешно отправлено');
                } else {
                    alert('Ошибка');
                }
            });
            return false;
        });
    }

    _initClasses() {
        new Cities();
    }
}

(function () {
    new Application();
})();
<?php

return [
    'Textpages' => 'Страницы',
    'Textpage' => 'Страница',
    'Categories' => 'Категории',
    'Category' => 'Категория',
    'Products' => 'Объекты',
    'Product' => 'Объект',
    'Designers' => 'Дизайнеры',
    'Designer' => 'Дизайнер',
    'Reviews' => 'Отзывы',
    'Review' => 'Отзыв',
    'Video Reviews' => 'Видео отзывы',
    'Video Review' => 'Видео отзыв',
    'Partners' => 'Партнеры',
    'Partner' => 'Партнер',
    'Videos' => 'Видео',
    'Video' => 'Видео',
    'Events' => 'События',
    'Event' => 'Событие',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];

?>
<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base-model class for table "personal".
 *
 * @property integer $id
 * @property string $name
 * @property string $function
 * @property string $image
 * @property integer $show_on_company
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_h1
 * @property string $seo_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $aliasModel
 */
abstract class Personal extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personal';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'function'], 'required'],
            [['show_on_company'], 'integer'],
            [['seo_description'], 'string'],
            [['name', 'function', 'image', 'seo_title', 'seo_keywords', 'seo_h1'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'function' => 'Должность',
            'image' => 'Изображение(180x180)',
            'show_on_company' => 'Показывать на странице О студии?',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_h1' => 'Seo H1',
            'seo_description' => 'Seo Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }




}

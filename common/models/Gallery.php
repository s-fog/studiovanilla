<?php

namespace common\models;

use Yii;
use \common\models\base\Gallery as BaseGallery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "gallery".
 */
class Gallery extends BaseGallery
{
    public $image_file;

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 ['image_file', 'image', 'extensions' => 'jpg, gif, png, jpeg']
             ]
        );
    }
}

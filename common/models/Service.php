<?php

namespace common\models;

use Yii;
use \common\models\base\Service as BaseService;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service".
 */
class Service extends BaseService
{
    public $image_file;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 ['image_file', 'image', 'extensions' => 'jpg, gif, png, jpeg']
             ]
        );
    }

    public function getPersonal() {
        return $this->hasMany(Personal::className(), ['id' => 'personal_id'])
            ->viaTable('personal_has_service', ['service_id' => 'id']);
    }
}

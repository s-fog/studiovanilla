<?php

namespace common\models;

use Yii;
use \common\models\base\Personal as BasePersonal;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "personal".
 */
class Personal extends BasePersonal
{
    public $image_file;

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => \voskobovich\linker\LinkerBehavior::className(),
                    'relations' => [
                        'service_ids' => 'services',
                    ],
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 ['image_file', 'image', 'extensions' => 'jpg, gif, png, jpeg'],
                 [['service_ids'], 'each', 'rule' => ['integer']]
             ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'service_ids' => 'Список услуг(Чтобы выбрать несколько, выбирайте с зажатой клавишой Ctrl)'
            ]
        );
    }

    public function getServices()
    {
        return $this->hasMany(Service::className(),['id' => 'service_id']
        )->viaTable('{{%personal_has_service}}',['personal_id' => 'id']);
    }
}

<?php

namespace common\models;

use Yii;
use \common\models\base\Serviceitem as BaseServiceitem;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "serviceitem".
 */
class Serviceitem extends BaseServiceitem
{
    public $image_file;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 ['image_file', 'image', 'extensions' => 'jpg, gif, png, jpeg']
             ]
        );
    }
}

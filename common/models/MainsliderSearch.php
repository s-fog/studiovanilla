<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Mainslider;

/**
* MainsliderSearch represents the model behind the search form about `common\models\Mainslider`.
*/
class MainsliderSearch extends Mainslider
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'price'], 'integer'],
            [['header', 'text', 'service', 'image'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Mainslider::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'service', $this->service])
            ->andFilterWhere(['like', 'image', $this->image]);

return $dataProvider;
}
}
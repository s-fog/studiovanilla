<?php

namespace common\models;

use Yii;
use \common\models\base\Mainslider as BaseMainslider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mainslider".
 */
class Mainslider extends BaseMainslider
{
    public $image_file;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
             parent::rules(),
             [
                 ['image_file', 'image', 'extensions' => 'jpg, gif, png, jpeg']
             ]
        );
    }
}

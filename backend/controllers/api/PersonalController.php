<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "PersonalController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PersonalController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Personal';
}

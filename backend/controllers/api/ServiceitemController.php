<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "ServiceitemController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class ServiceitemController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Serviceitem';
}

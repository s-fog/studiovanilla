<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Gallery $model
*/

$this->title = 'Создание';
?>
<div class="giiant-crud gallery-create">

    <h1>
        <?= Yii::t('models', 'Gallery') ?>
    </h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

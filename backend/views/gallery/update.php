<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Gallery $model
*/
    
$this->title = Yii::t('models', 'Gallery') . " " . $model->id . ', ' . 'Редактирование';
?>
<div class="giiant-crud gallery-update">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

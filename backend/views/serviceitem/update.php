<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Serviceitem $model
*/
    
$this->title = Yii::t('models', 'Serviceitem') . " " . $model->name . ', ' . 'Редактирование';
?>
<div class="giiant-crud serviceitem-update">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

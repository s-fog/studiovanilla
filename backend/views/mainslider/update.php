<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Mainslider $model
*/
    
$this->title = Yii::t('models', 'Mainslider') . " " . $model->id . ', ' . 'Редактирование';
?>
<div class="giiant-crud mainslider-update">

    <h1>
        <?= Yii::t('models', 'Mainslider') ?>
    </h1>

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

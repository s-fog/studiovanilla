<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Mainslider $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="mainslider-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Mainslider',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
<!-- attribute header -->
			<?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

<!-- attribute text -->
			<?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

<!-- attribute service -->
			<?= $form->field($model, 'service')->textInput(['maxlength' => true]) ?>

<!-- attribute price -->
			<?= $form->field($model, 'price')->textInput() ?>

<!-- attribute image -->
            <?=$this->render('@backend/views/blocks/image', [
                'form' => $form,
                'model' => $model,
                'image' => $model->image,
                'name' => 'image'
            ])?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Mainslider'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>


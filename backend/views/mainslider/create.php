<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Mainslider $model
*/

$this->title = 'Создание';
?>
<div class="giiant-crud mainslider-create">

    <h1>
        <?= Yii::t('models', 'Mainslider') ?>
    </h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Personal $model
*/
    
$this->title = Yii::t('models', 'Personal') . " " . $model->name . ', ' . 'Редактирование';
?>
<div class="giiant-crud personal-update">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

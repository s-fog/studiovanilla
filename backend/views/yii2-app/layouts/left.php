<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Главная страница', 'url' => ['/site/index']],
                    ['label' => 'Текстовые страницы', 'url' => ['/textpage/index']],
                    ['label' => 'Услуги', 'url' => ['/service/index']],
                    ['label' => 'Элементы услуг', 'url' => ['/serviceitem/index']],
                    ['label' => 'Персонал', 'url' => ['/personal/index']],
                    ['label' => 'Слайдер на Главной', 'url' => ['/mainslider/index']],
                    ['label' => 'Галерея', 'url' => ['/gallery/index']],
                ],
            ]
        ) ?>

    </section>

</aside>

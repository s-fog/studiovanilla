<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Service $model
*/
    
$this->title = Yii::t('models', 'Service') . " " . $model->name . ', ' . 'Редактирование';
?>
<div class="giiant-crud service-update">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use common\models\Serviceitem;
use nex\chosen\Chosen;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Service $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="service-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Service',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>
            

<!-- attribute name -->
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <!-- attribute alias -->
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<!-- attribute descr -->
			<?= $form->field($model, 'descr')->textarea(['rows' => 6]) ?>

<!-- attribute image -->
            <?=$this->render('@backend/views/blocks/image', [
                'form' => $form,
                'model' => $model,
                'image' => $model->image,
                'name' => 'image'
            ])?>

            <?/*= $form->field($model, 'serviceitem_ids')
                ->dropDownList(ArrayHelper::map(
                    Serviceitem::find()->select('id, name')->orderBy('name')->asArray()->all(),
                    'id',
                    'name'
                ), ['multiple' => true]) */?>

            <!-- attribute seo_description -->
            <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>

<!-- attribute seo_title -->
			<?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

<!-- attribute seo_keywords -->
			<?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>

<!-- attribute seo_h1 -->
			<?= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>

<!-- attribute serviceitem_list -->
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Service'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>


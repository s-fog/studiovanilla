<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Service $model
*/

$this->title = 'Создание';
?>
<div class="giiant-crud service-create">

    <h1>
        <?= Yii::t('models', 'Service') ?>
    </h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>

<?php

use common\models\Gallery;

$gallery = Gallery::find()->all();

?>
<div class="imageSlider">
    <div class="imageSlider__inner owl-carousel">
        <?php foreach($gallery as $item) { ?>
            <div class="imageSlider__item" style="background-image: url(<?=$item->image?>);"></div>
        <?php } ?>
    </div>
</div>
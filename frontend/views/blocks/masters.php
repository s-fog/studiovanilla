<?php

use common\models\Personal;

if (!isset($masters)) {
    $masters = Personal::find()->where(['show_on_company' => 1])->all();
}

?>
<div class="header2">Наши мастера</div>
<div class="masters">
    <div class="container">
        <div class="masters__inner">
            <?php foreach($masters as $master) { ?>
                <div class="masters__item">
                    <div class="masters__image" style="background-image: url(<?=$master->image?>);"></div>
                    <div class="masters__name"><?=$master->name?></div>
                    <div class="masters__function"><?=$master->function?></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
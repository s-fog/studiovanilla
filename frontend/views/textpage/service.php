<?php

$this->params['seo_title'] = $model->seo_title;
$this->params['seo_description'] = $model->seo_description;
$this->params['seo_keywords'] = $model->seo_keywords;
$this->params['name'] = $model->name;

?>

<div class="header2"><?=$model->name?></div>
<div class="servicePage">
    <div class="container">
        <div class="servicePage__inner">
            <?php foreach($serviceItems as $serviceItem) { ?>
                <div class="servicePage__item">
                    <div class="servicePage__itemName"><?=$serviceItem->name?></div>
                    <div class="servicePage__itemInfo">
                        <div class="servicePage__itemTime"><?=$serviceItem->time?></div>
                        <div class="servicePage__itemPrice"><?=$serviceItem->price?> Р</div>
                    </div>
                    <div class="servicePage__itemInner">
                        <div class="servicePage__itemImage" style="background-image: url(<?=$serviceItem->image?>);"></div>
                        <div class="servicePage__itemText"><?=$serviceItem->descr?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/masters', ['masters' => $model->personal])?>
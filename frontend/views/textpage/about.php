<?php

$this->params['seo_title'] = $model->seo_title;
$this->params['seo_description'] = $model->seo_description;
$this->params['seo_keywords'] = $model->seo_keywords;
$this->params['name'] = $model->name;

?>
<div class="header2">О студии</div>

<div class="advantages">
    <div class="advantages__inner">
        <div class="advantages__item">
            <div class="advantages__itemImage" style="background-image: url(/img/a1.png);"></div>
            <div class="advantages__itemInfo">
                <div class="advantages__itemHeader">Квалифицированные
                    специалисты</div>
                <div class="advantages__itemText">Приходите и убедитесь в профессионализме наших мастеров и доступности наших услуг!</div>
            </div>
        </div>
        <div class="advantages__item">
            <div class="advantages__itemImage" style="background-image: url(/img/a2.png);"></div>
            <div class="advantages__itemInfo">
                <div class="advantages__itemHeader">Лучшие материалы</div>
                <div class="advantages__itemText">Работаем только с лучшими материалами и напрямую с представителями марок,  что гарантирует качество материал.</div>
            </div>
        </div>
        <div class="advantages__item">
            <div class="advantages__itemImage" style="background-image: url(/img/a3.png);"></div>
            <div class="advantages__itemInfo">
                <div class="advantages__itemHeader">Безопасность</div>
                <div class="advantages__itemText">Все инструменты проходят обязательную стерилизацию, что гарантирует безопасность всех процедур.</div>
            </div>
        </div>
        <div class="advantages__item">
            <div class="advantages__itemImage" style="background-image: url(/img/a4.png);"></div>
            <div class="advantages__itemInfo">
                <div class="advantages__itemHeader">Онлайн-запись</div>
                <div class="advantages__itemText">В любое время Вы можете записаться на любую услугу нашей студии и моментально получить подтверждение с помощью «Онлайн-записи»</div>
            </div>
        </div>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/gallery')?>

<?=$this->render('@frontend/views/blocks/masters')?>
<?php

use frontend\models\CallbackForm;
use yii\widgets\ActiveForm;

$this->params['seo_title'] = $model->seo_title;
$this->params['seo_description'] = $model->seo_description;
$this->params['seo_keywords'] = $model->seo_keywords;
$this->params['name'] = $model->name;

?>
<div class="header2">Контакты</div>
<div class="contactsPage">
    <div class="contactsPage__inner">
        <div class="contactsPage__left">
            <div class="contactsPage__item contactsPage__text">Мы всегда рады видеть Вас в нашей студии. Если у Вас возник вопрос или Вам необходима консультация, звоните по указанному ниже номеру телефона:</div>
            <hr>
            <div class="contactsPage__item">
                <div class="contactsPage__header">АДРЕС:</div>
                <div class="contactsPage__text">г. Москва, ул. Дмитрия Ульянова, д. 2/22</div>
            </div>
            <div class="contactsPage__item">
                <div class="contactsPage__header">ТЕЛЕФОН:</div>
                <div class="contactsPage__text">+7 915 255-40-00</div>
            </div>
            <div class="contactsPage__item">
                <div class="contactsPage__header">ГРАФИК РАБОТЫ:</div>
                <div class="contactsPage__text">Понедельник - Пятница с 10:00 до 20:00<br>Суббота, Воскресенье с 11:00 до 19:00</div>
            </div>
        </div>
        <div class="contactsPage__right">
            <?php
            $callbackForm = new CallbackForm();
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'contactsForm sendForm',
                    'id' => 'contactsForm'
                ],
            ]);?>
            <div class="contactsForm__text">Вы можете воспользоваться формой обратной<br>связи, чтобы задать интересующий Вас вопрос</div>
            <div class="contactsForm__inner">
                <?=$form->field($callbackForm, 'name')
                    ->textInput([
                        'class' => 'contactsForm__input'
                    ])?>
                <?=$form->field($callbackForm, 'phone')
                    ->textInput([
                        'class' => 'contactsForm__input'
                    ])?>

                <?=$form->field($callbackForm, 'email')
                    ->textInput([
                        'class' => 'contactsForm__input'
                    ])?>
                <?=$form->field($callbackForm, 'message')
                    ->textarea([
                        'class' => 'contactsForm__input'
                    ])?>
            </div>
            <button type="submit" class="contactsForm__submit">Отправить!</button>

            <?=$form->field($callbackForm, 'type')
                ->hiddenInput([
                    'value' => 'Заказан обратный звонок на сайте Studiovanilla'
                ])->label(false)?>

            <?=$form->field($callbackForm, 'BC')
                ->hiddenInput([
                    'value' => ''
                ])->label(false)?>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>

<div class="header2">Мы на карте</div>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div id="map"></div>
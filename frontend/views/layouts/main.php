<?php

use common\models\Service;
use common\models\Textpage;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1070">
    <?= Html::csrfMetaTags() ?>
    <title><?=(isset($this->params['seo_title']) && !empty($this->params['seo_title']))? $this->params['seo_title'] : $this->params['name'].' | Studiovanilla'?></title>
    <?=(isset($this->params['seo_description']) && !empty($this->params['seo_description']))? '<meta name="description" content="'.$this->params['seo_description'].'">' : ''?>
    <?=(isset($this->params['seo_keywords']) && !empty($this->params['seo_keywords']))? '<meta name="keywords" content="'.$this->params['seo_keywords'].'">' : ''?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="mainHeader">
    <div class="container">
        <div class="mainHeader__inner">
            <div class="mainHeader__left">
                <a href="https://www.instagram.com/vanilla_nails_beauty/" target="_blank" rel="nofollow" class="mainHeader__leftItem insta"></a>
            </div>
            <a href="/" class="mainHeader__logo"></a>
            <div class="mainHeader__contacts">
                <a href="tel:+7 915 255-40-00" class="mainHeader__contactsPhone">+7 915 255-40-00</a>
                <address class="mainHeader__contactsAddress">Москва, ул. Дмитрия Ульянова, д.2/22</address>
                <div class="son_wrapper">
                    <div class="son_outercircle"></div>
                    <a href="https://widget3.sonline.su/ru/services/?placeid=5375" rel="nofollow" target="_blank">
                        <span class="son_circle"><br>Онлайн<br/> запись</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu">
    <div class="container">
        <div class="menu__inner">
            <div class="menu__left"></div>
            <ul class="menu__middle">
                <?php foreach(Service::find()->all() as $sercice) {
                    echo ' <li><a href="'.Url::to(['textpage/index', 'alias' => $sercice->alias]).'" class="menu__link">'.$sercice->name.'</a></li>';
                } ?>
            </ul>
            <ul class="menu__right">
                <?=Textpage::liLink(1, 'menu__link menu__link2')?>
                <?=Textpage::liLink(2, 'menu__link menu__link2')?>
            </ul>
        </div>
    </div>
</div>

<?=$content?>

<div class="footer">
    <div class="container">
        <div class="footer__inner">
            <a href="/" class="footer__logo"></a>
            <ul class="footer__menu">
                <?php foreach(Service::find()->all() as $sercice) {
                    echo ' <li><a href="'.Url::to(['textpage/index', 'alias' => $sercice->alias]).'" class="footer__menuLink">'.$sercice->name.'</a></li>';
                } ?>
            </ul>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

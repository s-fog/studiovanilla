<?php

$this->params['seo_title'] = $model->seo_title;
$this->params['seo_description'] = $model->seo_description;
$this->params['seo_keywords'] = $model->seo_keywords;
$this->params['name'] = $model->name;

?>
<div class="slider">
    <div class="container">
        <div class="slider__inner owl-carousel">
            <?php use yii\helpers\Url;

            foreach($mainSlider as $item) { ?>
                <div class="slider__item" style="background-image: url(<?=$item->image?>);"></div>
            <?php } ?>
        </div>
        <div class="slider__text owl-carousel">
            <?php foreach($mainSlider as $item) { ?>
                <div class="slider__textItem">
                    <div class="slider__textItemHeader"><?=$item->header?></div>
                    <div class="slider__textItemText"><?=$item->text?></div>
                    <div class="slider__textItemName"><?=$item->service?></div>
                    <div class="slider__textItemPrice"><?=$item->price?> Р</div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="services">
    <div class="header">Основные услуги</div>
    <div class="container">
        <div class="services__inner">
            <?php foreach($services as $service) {
                $url = Url::to(['textpage/index', 'alias' => $service->alias]);
                ?>
                <div class="services__item">
                    <a href="<?=$url?>" class="services__itemImage" style="background-image: url(<?=$service->image?>);"></a>
                    <a href="<?=$url?>" class="services__itemName"><span><?=$service->name?></span></a>
                    <div class="services__itemText"><?=$service->descr?></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?=$this->render('@frontend/views/blocks/gallery')?>

<div class="header">Дополнительные услуги</div>
<div class="dopServices">
    <div class="dopServices__item">Лечение ногтей. Система IBX</div>
    <div class="dopServices__item">Уход для рук и ног</div>
    <div class="dopServices__item">Коррекция бровей</div>
    <div class="dopServices__item">Экспресс укладка</div>
</div>
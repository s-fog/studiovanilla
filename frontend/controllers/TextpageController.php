<?php
namespace frontend\controllers;

use common\models\Service;
use common\models\Serviceitem;
use common\models\Textpage;
use HttpException;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class TextpageController extends Controller
{
    public function actionIndex($alias)
    {
        if ($model = Textpage::find()->where(['alias' => $alias])->one()) {
            $view = '';

            if ($model->id == 1) {
                $view = 'about';
            } else if ($model->id == 2) {
                $view = 'contacts';
            }

            return $this->render($view, [
                'model' => $model,
            ]);
        }

        if ($model = Service::find()->where(['alias' => $alias])->one()) {
            $serviceItems = Serviceitem::find()->where(['service_id' => $model->id])->all();

            return $this->render('service', [
                'model' => $model,
                'serviceItems' => $serviceItems,
            ]);
        }

        throw new NotFoundHttpException();
    }
}

<?php

namespace frontend\models;

use Yii;

class CallbackForm extends Forms
{
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

}